import * as http from "http";
import * as httpProxy from "http-proxy";

let proxy = httpProxy.createProxyServer({});

const PORT = 8009;
const TARGET_HOST = 'http://google.com';

async function startServer() {
  http.createServer(function (req, res) {
    proxy.web(req, res, {
      target: TARGET_HOST
    });
    proxy.on('proxyReq', (req) => {
      console.log(`[${new Date()}] Request method: ${req.method}, path: ${req.path}`);
    });
  }).listen(PORT);
}

(async () => {
  await startServer();
  console.log('----');
  console.log('Proxy started:');
  console.log(`IP: http://localhost:${PORT}`);
  console.log('----');
  process.stdin.resume();
})();